<?php
function findMin($array){
    if(is_array($array) && count($array) !== 0){
        return min($array);
    }
    return "Invalid input";
}

echo "Min of [2,6,-8,5,0] -> " . findMin([2,6,-8,5,0]);
<?php
function findMax($array){
    if(is_array($array) && count($array) !== 0){
        return max($array);
    }
    return "Invalid input";
}

echo "Max of [2,6,-8,5,0] -> " . findMax([2,6,-8,5,0]);
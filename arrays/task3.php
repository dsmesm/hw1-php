<?php
function findMinIndex($array){
    if(is_array($array) && count($array) !== 0){
        return array_search(min($array), $array);
    }
    return "Invalid input";
}

echo "Index of min of [2,6,-8,5,0] -> " . findMinIndex([2,6,-8,5,0]);
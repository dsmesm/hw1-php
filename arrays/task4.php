<?php
function findMaxIndex($array){
    if(is_array($array) && count($array) !== 0){
        return array_search(max($array), $array);
    }
    return "Invalid input";
}

echo "Index of min of [2,6,-8,5,0] -> " . findMaxIndex([2,6,-8,5,0]);
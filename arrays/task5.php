<?php
function arraySum($array){
    return array_reduce($array, function ($sum, $item){
        $sum += $item;
        return $sum;
    });
}

echo "Sum of [2,6,8,5] -> " . arraySum([2,6,8,5]);
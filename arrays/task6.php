<?php
function arrayReverse($array){
    return array_reverse($array);
}

echo "Reverse of [2,6,-8,5,0] -> " .implode(', ',arrayReverse([2,6,-8,5,0]));
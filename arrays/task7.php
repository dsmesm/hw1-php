<?php
function countOdd($array){
    return array_reduce($array, function ($odds, $item){
        if($item%2 !== 0){
            return ++$odds;
        }else{
            return $odds;
        }
    });
}

echo "Number of odds in [1,2,3,8,5] -> " . countOdd([1,2,3,8,5]);

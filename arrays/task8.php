<?php
function swap($array){
    $half1 = array_slice($array, (count($array)/2));
    $half2 = array_slice($array, 0, (count($array)/2));
    return array_merge($half1, $half2);
}

echo "Swap [1,2,3,8,5] -> " . implode(', ', swap([1,2,3,8,5]));
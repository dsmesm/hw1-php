<?php
function bubbleSort($array){
            for ($i = 0; $i < count($array); $i++) {
                for ($j = 0; $j < count($array) - $i - 1; $j++) {
                    if ($array[$j] > $array[$j + 1]) {
                        $buf = $array[$j];
                        $array[$j] = $array[$j + 1];
                        $array[$j + 1] = $buf;
                    }
                }
            }
            return $array;
}

function selectSort($array)
{
    for($i=0; $i<count($array)-1; $i++) {
        $min = $i;
        for($j=$i+1; $j<count($array); $j++) {
            if ($array[$j]<$array[$min]) {
                $min = $j;
            }
        }
        $array = swap_positions($array, $i, $min);
    }
    return $array;
}

function swap_positions($data1, $left, $right) {
    $temp = $data1[$right];
    $data1[$right] = $data1[$left];
    $data1[$left] = $temp;
    return $data1;
}

function insertSort($array){
    for($i=0;$i<count($array);$i++){
        $val = $array[$i];
        $j = $i-1;
        while($j>=0 && $array[$j] > $val){
            $array[$j+1] = $array[$j];
            $j--;
        }
        $array[$j+1] = $val;
    }
    return $array;
}

echo "Array: ".[2,6,-8,5,0];
echo "<br>Bubble: ".implode(', ',bubbleSort([2,6,-8,5,0]));
echo "<br>Select: ".implode(', ',selectSort([2,6,-8,5,0]));
echo "<br>Insert: ".implode(', ',insertSort([2,6,-8,5,0]));
<?php
function getString($number)
{
    $str = "";
    $hundreds = 0;
    $tens = 0;
    $ones = 0;
    $numbers = ["one ", "two ", "three ", "four ", "five ", "six ", "seven ", "eight ", "nine ", "ten ", "eleven ",
        "twelve ", "thirteen ", "fourteen ", "fifteen ", "sixteen ", "seventeen ", "eighteen ", "nineteen "];
    $numbers2 = ["twenty ", "thirty ", "forty ", "fifty ", "sixty ", "seventy ", "eighty ", "ninety "];
    if ($number === 0) {
        return "zero";
    }
    if ($number / 100 >= 1) {
        $hundreds = (int)($number / 100) - 1;
        $str .= "$numbers[$hundreds] hundred ";
    }
    if (($number - ($hundreds + 1) * 100) / 10 >= 1) {
        $tens = (int)(($number - ($hundreds + 1) * 100) / 10);
        if ($tens >= 2) {
            $str .= $numbers2[$tens - 2];
        }
    }
    if ($number - ($hundreds + 1) * 100 - $tens * 10 >= 1) {
        $ones = $number - ($hundreds + 1) * 100 - $tens * 10;
        if ($tens * 10 + $ones < 20) {
            $str .= $numbers[$tens * 10 + $ones - 1];
        } else {
            $str .= $numbers[$ones - 1];
        }
    }
    return $str;
}

echo "539 is " . getString(539);
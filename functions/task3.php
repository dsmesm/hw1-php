<?php
function getNumber($string){
    $ones = ["one" , "two", "three" , "four", "five", "six", "seven", "eight", "nine", "ten", "eleven",
        "twelve", "thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"];
    $tens = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];
    $hundred = "hundred";
    $number = 0;
        $string = trim($string);
            $temp = "";
            if($string == "zero"){
                return $number;
            }
            if(strpos($string, $hundred) !== false){
                for($i = 0; $i < strlen($string);$i++){
                    if($string[$i] !== ' '){
                            $temp .= $string[$i];
                        }else{
                            break;
                    }
                }
                $string = str_replace("hundred","", $string);
                $string = str_replace($temp,"", $string);
                trim($string);
                $number += (array_search(trim($temp), $ones) + 1)*100;
            }
            for($i = 0;$i<count($tens);$i++){
                if(strpos($string, $tens[$i]) !== false){
                    $number += ($i+2)*10;
                }
            }
            for($i = 0;$i<count($ones);$i++){
                if(strpos($string, $ones[$i]) !== false){
                    $number += ($i+1);
                }
            }
        return $number;
}

echo "five hundred forty two is  ".getNumber("five hundred forty two");

<?php

function calc($a, $b)
{
    if (is_finite($a) && is_finite($b)) {
        if ($a % 2 === 0) {
            return $a * $b;
        } else {
            return $a + $b;
        }
    } else {
        return "Invalid input";
    }
}

echo "Input: 2,5 -> " . calc(2, 5);
echo "<br>Input: 3,5 -> " . calc(3, 5);
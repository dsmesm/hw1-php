<?php
function point($x,$y) {
    if((is_integer($x) && is_finite($x)) && (is_integer($y) && is_finite($y))){
        if($x > 0 && $y >0){
            return "I quarter";
    }else if($x >0 && $y < 0){
            return "IV quarter";
    }else if($x < 0 && $y > 0){
            return "II quarter";
    }else if($x < 0 && $y < 0){
            return "III quarter";
    }else{
            if($x == 0){
                return "Y axis";
            }else if($y == 0){
                    return "X axis";
            }else{
                    return "Center";
            }
        }
    }else{
        return "Invalid input";
    }
}

echo "Input: 2,5 -> " . point(2, 5);
echo "<br>Input: -5,8 -> " . point(-5, 8);
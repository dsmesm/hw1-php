<?php
function calc2($a,$b,$c){
    if((is_integer($a) && is_finite($a)) &&
        (is_integer($b) && is_finite($b)) &&
        (is_integer($c) && is_finite($c))){

        $sum = 0;
        if($a >= 0) { $sum += $a; }
        if($b >= 0) { $sum += $b; }
        if($c >= 0) { $sum += $c; }
        return $sum;
    }else{
        return "Invalid input";
    }
}

echo "Input: 2,5,8 -> " . calc2(2,5,8);
echo "<br>Input: -5,8,7 -> " . calc2(-5, 8,10);
<?php
function findMax($a,$b,$c) {
    if((is_integer($a) && is_finite($a)) &&
        (is_integer($b) && is_finite($b)) &&
        (is_integer($c) && is_finite($c))){

        $product = $a*$b*$c;
        $sum = $a+$b+$c;
        if($product > $sum){
            return $product + 3;
        }else{
            return $sum + 3;
        }
    }else{
        return "Invalid input";
    }
}

echo "Input: 2,5,8 -> " . findMax(2,5,8);
echo "<br>Input: -5,8,7 -> " . findMax(-5, 8,10);
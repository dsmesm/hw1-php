<?php
function grade($g) {
    if(is_integer($g)){
        if($g >= 0 && $g <= 19){
            return "F";
    }else if($g >= 20 && $g <= 39){
            return "E";
    }else if($g >= 40 && $g <= 59){
            return "D";
    }else if($g >= 60 && $g <= 74){
            return "C";
    }else if($g >= 75 && $g <= 89){
            return "B";
    }else if($g >= 90 && $g <= 100){
            return "A";
    }else{
            return "Wrong grade";
    }
    }else{
        return "Invalid input";
    }
}

echo "Input: 25 -> " . grade(25);
echo "<br>Input: 85 -> " . grade(85);
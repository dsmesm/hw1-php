<?php
function _count() {
    $sum = 0;
    $quantity = 0;
    for($i = 1; $i <= 99; $i++) {
        if($i%2 === 0) {
            $sum += $i;
            $quantity++;
        }
    }
    return [$sum, $quantity];
}

echo "Sum and quantity: ".implode(' ,', _count());
<?php
function prime($number) {
    if(is_integer($number)&& is_finite($number)){
        $flag = 0;
        for($i = 1; $i <= $number; $i++) {
            if($number%$i == 0) {
                $flag++;
            }
        }
        if($flag == 2) {
            return "Is prime number";
        }else{
            return "Not a prime number";
        }
    }else{
        return "Not a number";
    }
}

echo "Input: 2 -> " . prime(2);
echo "<br>Input: 4 -> " . prime(4);
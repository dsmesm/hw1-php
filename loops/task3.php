<?php
function s_root($number) {
    if(is_integer($number)&& is_Finite($number) && $number >=0){
        $result_s = 0;
        for(; $result_s*$result_s < $number; $result_s++){}
        //return `Sequential root: ${result_s - 1}`
        return $result_s;
    }else{
        return "Invalid input";
    }
}
function b_root($number) {
    if(is_integer($number) == "number" && is_Finite($number) && $number >=0){
        $l = 0;
        $r = $number;
        $half = 0;
        $temp = 0;
        while(true) {
            $half = (($r + $l) / 2);
            if ($temp == $half){
                //return `Binary root: ${half}`
                return (int)$half;
            }else{
                if (($half * $half) >= $number){
                    $r = $half;
                }else{
                    $l = $half;
                }
                $temp = $half;
            }
        }
    }else{
        return "Invalid input";
    }
}

echo "s_root(sequential): 144 -> " . s_root(144);
echo "<br>b_root(binary): 144 -> " . b_root(144);
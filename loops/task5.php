<?php
function sum($number) {
    if(is_integer($number) && is_Finite($number)){
        $str = (string)(abs($number));
        $sum = 0;
        for($i = 0; $i < strlen($str); $i++) {
            $sum += (int)($str[$i]);
        }
        return $sum;
    }else{
        return "Invalid input";
    }
}

echo "Input: 135 -> " . sum(135);
<?php
function mirror($number) {
    if(is_integer($number) && is_Finite($number) && $number >= 0){
        $str = (string)($number);
        $mirrored = "";
        for($i = strlen($str) - 1; $i >= 0; $i--) {
            $mirrored .= $str[$i];
        }
        return (int)($mirrored);
    }else{
        return "Invalid input";
    }
}

echo "Input: 135 -> " . mirror(135);